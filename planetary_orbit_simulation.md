# Let's build a Solar System

**Topics**: `astronomy`, `computational physics`, `graphics`, `calculus`

## Project proposal
+ Understand the basic physics in planetary interactions: gravitational force, energy, momentum, etc.
+ Grasp some concepts of calculus: derivatives, differential equations
+ Explore interactively how the Solar System works (ideally, at least)


### Resources
+ [[**main**] Let's build a Solar System (series)](https://youtu.be/4ycpvtIio-o)
+ [Euler-Cromer method for begginers](https://youtu.be/rPkJtpVJwSw)
+ [Semi-implicit Euler-Cromer method](https://en.wikipedia.org/wiki/Semi-implicit_Euler_method)
+ [The essence of calculus](https://youtu.be/WUvTyaaNkzM)
+ [3blue1brown - Overview of differential Equations](https://youtu.be/p_di4Zn4wz4)
