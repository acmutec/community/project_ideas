
## Minecraft Clone with C++ and OpenGL4

Create a basic engine for a minecraft-like game, including random terrain generation in 3D using Simplex Noise, 3D rendering, Dynamic lightning, skyboxes, shaders and more!
We'll develop the entire engine using C++ and some libraries (Not reinventing the wheel) including:
* OpenGL4 (GPU Rendering)
* SOIL (Image Loading)
* GLFW (Windowing)
* ASSIMP (Model Loading)
* GLAD (Check resources)
* CMake (Build/Compiling)
* imgui (basic GUI for debugging and testing)

## Topics (Not in Order)
### Rendering
* Basic
	* Rendering
	* Lightning
	* GLSL Shader Programming
	* Camera
* Intermediate
	* 3D Transformations / Movement
	* Dynamic Lightning
	* Depth Testing
	* Stencil Testing
	* Framebuffers
	* Blending
	* Cubemaps
	* Shadow Mapping
	* Normal Mapping
* Advanced
	* Screen-Space Ambient Occlusion
	* HDR
	* PBR
	* LOD
### Procedural Generation
* Random 3D Terrain Generation with SimplexNoise
* Tesselation
* Fog (Not strictly related)

## Resources by Topic
* **Engine Reference (WIP)** 
	* https://github.com/pieromarini/Cpp-Graphics-Engine

* **Linear Algebra (DON'T SKIP)**
	* https://www.youtube.com/playlist?list=PLZHQObOWTQDPD3MizzM2xVFitgF8hE_ab
	* https://www.youtube.com/watch?v=vQ60rFwh2ig
	* http://lazyfoo.net/articles/article10/index.php 

* **Windowing (GLFW)** 
    * https://www.glfw.org/


* **Rendering (OpenGL)**
    * https://learnopengl.com/
    * https://open.gl/
    * https://www.opengl.org/

   
* **Shaders (GLSL)**
	* https://learnopengl.com/Getting-started/Shaders
    
    
* **Procedural Generation**
    * https://en.wikipedia.org/wiki/Perlin_noise
    * https://notch.tumblr.com/post/3746989361/terrain-generation-part-1
    * http://weber.itn.liu.se/~stegu/simplexnoise/simplexnoise.pdf
    
* **Misc**
	* [GLFW (OpenGL Loading)](https://www.khronos.org/opengl/wiki/OpenGL_Loading_Library)
	* [imgui](https://github.com/ocornut/imgui)
	* [SOIL](http://www.lonesock.net/soil.html)
	* [CMake](https://cmake.org/)
	* [ASSIMP](http://www.assimp.org/)