# Genetic Algorithms

**Topics**: `artificial intelligence`, `evolutionary computation`, `algorithms`

## Project proposal
+ Understand the basics of local search algorithms
+ Use simmulated annealing derivates to approximate global maximum
+ Get introduced to the world of AI + Biology
+ `[Cool bonus]` Run visual simulation

## Algorithm
```
START
population <- generate_initial_population()
compute_fitness()
WHILE population has not converged
    selection()
    crossover()
    mutation()
    compute_fitness()
ENDWHILE
END
```

### Resources
+ [MIT OCW - Genetic algorithms](https://www.youtube.com/watch?v=kHyNqSnzP8Y)
+ [List of Genetic Algorithms Applications](https://en.wikipedia.org/wiki/List_of_genetic_algorithm_applications)
